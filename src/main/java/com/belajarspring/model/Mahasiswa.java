package com.belajarspring.model;

import java.io.Serializable;

public class Mahasiswa implements Serializable {
	
	private static final long serialVersionUID = 1L;
	int nim;
	String nama;
	String gender;
	String status;
	public Mahasiswa() {
		super();
	}
	public Mahasiswa(int nim, String nama, String gender, String status) {
		super();
		this.nim = nim;
		this.nama = nama;
		this.gender = gender;
		this.status = status;
	}
	public int getNim() {
		return nim;
	}
	public void setNim(int nim) {
		this.nim = nim;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}
	
	
	
}
