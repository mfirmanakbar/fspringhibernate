package com.belajarspring.dao;

import java.util.List;

import com.belajarspring.model.Mahasiswa;

public interface MahasiswaDao {
	public void insert(Mahasiswa mahasiswa);
	public Mahasiswa findByMahasiswaId(int nim);
	public List<Mahasiswa> findAllMahasiswa();
	public void update(Mahasiswa mahasiswa);
	public void delete(int nim);
	public void nativeInsert(Mahasiswa mahasiswa);
	public List<Mahasiswa> nativeFindAllMahasiswa();
}
