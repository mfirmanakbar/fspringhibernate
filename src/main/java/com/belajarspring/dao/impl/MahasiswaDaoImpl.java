package com.belajarspring.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import com.belajarspring.dao.MahasiswaDao;
import com.belajarspring.model.Mahasiswa;

public class MahasiswaDaoImpl implements MahasiswaDao {
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public void insert(Mahasiswa mahasiswa) {
		this.sessionFactory.getCurrentSession().save(mahasiswa);
		
	}
	
	/*Perhatikan kode program di bawah ini:
		.createQuery("from Mahasiswa where nim=:nims")
		mengapa from Mahasiswa bukankan nama tabel tb_mahasiswa ?
		karena cara ini bukan native, tidak langsung ke tabel tapi diarahkan ke JavaModel
		yang mana JavaModel sudah disetting oleh Mahasiswa.hbm.xml
	*/	
	@SuppressWarnings("unchecked")
	@Transactional
	public Mahasiswa findByMahasiswaId(int nim) {
		List<Mahasiswa> list = this.sessionFactory.getCurrentSession()
                .createQuery("from Mahasiswa where nim=:nims")
                .setParameter("nims", nim)
                .list(); 	
		return (Mahasiswa)list.get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Mahasiswa> findAllMahasiswa() {
		List<Mahasiswa> custList = new ArrayList<Mahasiswa>();
		custList = this.sessionFactory.getCurrentSession()
                .createQuery("from Mahasiswa")
                .list(); 	
		return custList;
	}

	@Transactional
	public void update(Mahasiswa mahasiswa) {
		this.sessionFactory.getCurrentSession().update(mahasiswa);
	}

	@Transactional
	public void delete(int nim) {
		Mahasiswa mahasiswa = findByMahasiswaId(nim);
		this.sessionFactory.getCurrentSession().delete(mahasiswa);
	}

	@Transactional
	public void nativeInsert(Mahasiswa mahasiswa) {
		Query query = this.sessionFactory.getCurrentSession().createSQLQuery("INSERT INTO tb_mahasiswa (nim, nama, gender, status) " +
				"VALUES (:nim, :nama, :gender, :status)"	)
				.setParameter("nim", mahasiswa.getNim())
				.setParameter("nama", mahasiswa.getNama())
				.setParameter("gender", mahasiswa.getGender())
				.setParameter("status", mahasiswa.getStatus());

		int affectedRow = query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Mahasiswa> nativeFindAllMahasiswa() {
		Query query = this.sessionFactory.getCurrentSession()
				.createSQLQuery("SELECT nim, nama, gender, status FROM tb_mahasiswa")
				.addEntity(Mahasiswa.class);
		List<Mahasiswa> mahasiswaList = query.list();
		return mahasiswaList;
	}

}
