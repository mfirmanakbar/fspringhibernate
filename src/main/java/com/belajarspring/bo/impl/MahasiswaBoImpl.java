package com.belajarspring.bo.impl;

import java.util.List;

import com.belajarspring.bo.MahasiswaBo;
import com.belajarspring.dao.MahasiswaDao;
import com.belajarspring.model.Mahasiswa;

public class MahasiswaBoImpl implements MahasiswaBo{
	private MahasiswaDao mahasiswaDao;

	public MahasiswaDao getMahasiswaDao() {
		return mahasiswaDao;
	}

	public void setMahasiswaDao(MahasiswaDao mahasiswaDao) {
		this.mahasiswaDao = mahasiswaDao;
	}

	public void insert(Mahasiswa mahasiswa) {
		mahasiswaDao.insert(mahasiswa);
	}

	public Mahasiswa findByMahasiswaId(int nim) {
		return mahasiswaDao.findByMahasiswaId(nim);
	}

	public List<Mahasiswa> findAllMahasiswa() {
		return mahasiswaDao.findAllMahasiswa();
	}

	public void update(Mahasiswa mahasiswa) {
		mahasiswaDao.update(mahasiswa);		
	}

	public void delete(int nim) {
		mahasiswaDao.delete(nim);
	}

	public void nativeInsert(Mahasiswa mahasiswa) {
		mahasiswaDao.nativeInsert(mahasiswa);
	}

	public List<Mahasiswa> nativeFindAllMahasiswa() {
		return mahasiswaDao.nativeFindAllMahasiswa();
	}



}
