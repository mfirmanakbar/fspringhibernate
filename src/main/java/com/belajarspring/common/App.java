package com.belajarspring.common;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.belajarspring.bo.MahasiswaBo;
import com.belajarspring.model.Mahasiswa;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ){
        //System.out.println( "Hello World! FSpringHibernate" );
    	
    	ApplicationContext appContext = new ClassPathXmlApplicationContext("spring/config/Main.xml");

    	MahasiswaBo mhsBo = (MahasiswaBo)appContext.getBean("mahasiswaBo");
    	
    	/*Begin Part 1 - INSERT*/
    	mhsBo.insert(new Mahasiswa(1,"Muhammad","L","On"));
    	mhsBo.insert(new Mahasiswa(2,"Firman","L","On"));
    	mhsBo.insert(new Mahasiswa(3,"Ak","P","Off"));
    	/*Ending Part 1*/
    	
    	/*Begin Part 2 - RETRIEVE Mahasiswa dengan Nim=1*/
    	Mahasiswa mahasiswax = mhsBo.findByMahasiswaId(1);
    	System.out.println("Hai Bro: "
    			+mahasiswax.getNim()+", "
    			+mahasiswax.getNama()+", "
    			+mahasiswax.getGender()+", "
    			+mahasiswax.getStatus());
    	/*Ending Part 2*/
    	
    	/*Begin Part 3 - UPDATE Mahasiswa dengan Nim=3*/
    	Mahasiswa mahasiswax2 = mhsBo.findByMahasiswaId(3);
    	System.out.println("Hai Bro, sebelum update: "
    			+mahasiswax2.getNim()+", "
    			+mahasiswax2.getNama()+", "
    			+mahasiswax2.getGender()+", "
    			+mahasiswax2.getStatus());    	
    	mahasiswax2.setNama("Akbar");
    	mahasiswax2.setGender("L");
    	mhsBo.update(mahasiswax2);
    	System.out.println("Hai Bro, setelah update: "
    			+mahasiswax2.getNim()+", "
    			+mahasiswax2.getNama()+", "
    			+mahasiswax2.getGender()+", "
    			+mahasiswax2.getStatus());
    	/*Ending Part 3*/
    	
    	/*Begin Part 4 - DELETE Mahasiswa dengan Nim=2*/    	
    	mhsBo.delete(2);
    	/*Ending Part 4*/
    	
    	/*Begin Part 5 - DISPLAY*/
    	System.out.println("-----------------------------------------------");
    	List<Mahasiswa> mhsList = mhsBo.findAllMahasiswa();
    	for(Mahasiswa mx:mhsList){
    		System.out.println("Hai Bro, Munculin: "
        			+mx.getNim()+", "
        			+mx.getNama()+", "
        			+mx.getGender()+", "
        			+mx.getStatus());
    	}
    	/*Ending Part 5*/
    	    	
    	/*Begin Part 6 - NATIVE INSERT & NATIVE DISPLAY*/
    	mhsBo.nativeInsert(new Mahasiswa(4,"Hani","P","On"));
    	mhsBo.nativeInsert(new Mahasiswa(5,"DS","P","Off"));
        System.out.println("-----------------------------------------------");
        List<Mahasiswa> xmhsList = mhsBo.nativeFindAllMahasiswa();
        for(Mahasiswa xm:xmhsList){
        	System.out.println("Hai Bro, Munculin Native: "
        			+xm.getNim()+", "
        			+xm.getNama()+", "
        			+xm.getGender()+", "
        			+xm.getStatus());
        }
        /*Ending Part 6*/
    }
}
